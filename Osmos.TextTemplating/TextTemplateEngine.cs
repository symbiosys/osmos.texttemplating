﻿using Microsoft.VisualStudio.TextTemplating;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Osmos.TextTemplating
{
    public static class TextTemplateEngine
    {
        public static async Task<string> GetTextAsync(string templateFilePath, TextTempateHost host)
        {
            if (!File.Exists(templateFilePath))
                throw new FileNotFoundException(string.Format("Can not find the text template file at \"{0}\"", templateFilePath));

            var engine = new Engine();
            string entityTemplate = await ReadFileAsync(templateFilePath);
            host.TemplateFile = templateFilePath;
            string contents = engine.ProcessTemplate(entityTemplate, host);
            return contents;
        }

        public static async Task GetFileAsync(
            string templateFilePath, 
            string destinationFolderPath, 
            string destinationFileName, 
            TextTempateHost host)
        {
            if (!Directory.Exists(destinationFolderPath))
                throw new FileNotFoundException(string.Format("Can not find the destination folder at \"{0}\"", destinationFolderPath));

            string gerneratedText = await GetTextAsync(templateFilePath, host);

            string filePath = Path.Combine(destinationFolderPath, destinationFileName + host.FileExtension);

            await WriteFileAsync(filePath, gerneratedText);
        }

        public static async Task WriteFileAsync(string filePath, string text)
        {
            if (File.Exists(filePath)) {
                File.Delete(filePath);
            }
            File.Create(filePath);

            byte[] encodedText = Encoding.Unicode.GetBytes(text);

            // for some mysterious reason (probably because of the async) it cant write the file just after deleting it
            // => throws : can not open the file, because its used by another process
            // thats the explanation for this try catch loop
            bool test;
            do
            {
                try
                {
                    test = false;
                    using (FileStream sourceStream = new FileStream(filePath,
                    FileMode.Append, FileAccess.Write, FileShare.None,
                    bufferSize: 4096, useAsync: true))
                    {
                        await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
                    };
                }
                catch (IOException )
                {
                    test = true;
                }
            } while (test);
        }

        private static async Task<string> ReadFileAsync(string filePath) {
            using (FileStream sourceStream = new FileStream(filePath,
        FileMode.Open, FileAccess.Read, FileShare.Read,
        bufferSize: 4096, useAsync: true))
            {
                string result;
                using (StreamReader reader = File.OpenText(filePath))
                {
                    result = await reader.ReadToEndAsync();
                }
                return result;
            }
        }
    }
}
