﻿using Microsoft.VisualStudio.TextTemplating;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Osmos.TextTemplating
{
    public class TextTempateHostOptions{
        public IList<string> StandardAssemblyReferences { get; set; }
        public IList<string> StandardImports { get; set; }
        public object Object { get; set; }
    }

    public class TextTempateHost : ITextTemplatingEngineHost
    {
        public object Object { get { return _object; } }

        public TextTempateHost(TextTempateHostOptions options = null)
        {
            if (options != null) {
                _standardAssemblyReferences = options.StandardAssemblyReferences;
                _standardImports = options.StandardImports;
                _object = options.Object;
            }
        }

        IList<string> _standardAssemblyReferences = null;
        IList<string> _standardImports = null;
        CompilerErrorCollection _errors = null;
        object _object = null;

        public string FileExtension { get; set; }
        public Encoding OutputEncoding { get; set; }

        public CompilerErrorCollection Errors { get { return _errors; } }

        IList<string> ITextTemplatingEngineHost.StandardAssemblyReferences
        {
            get
            {
                var standardAssemblyReferences = new List<string> { 
                    //Assembly.GetExecutingAssembly().Location,
                    typeof(Uri).Assembly.Location,
                    typeof(Enumerable).Assembly.Location
                };

                if (_standardAssemblyReferences != null && _standardAssemblyReferences.Any()) {
                    standardAssemblyReferences.AddRange(_standardAssemblyReferences);
                }

                return standardAssemblyReferences.ToArray();
            }
        }

        IList<string> ITextTemplatingEngineHost.StandardImports
        {
            get
            {
                var standardImports = new List<string> { 
                    "System"
                };

                if (_standardImports != null && _standardAssemblyReferences.Any()) {
                    standardImports.AddRange(_standardImports);
                }

                return standardImports.ToArray();
            }
        }

        public string TemplateFile { get; set; }

        object ITextTemplatingEngineHost.GetHostOption(string optionName)
        {
            if (optionName == "CacheAssemblies")
            {
                return 1;
            }

            return null;
        }

        bool ITextTemplatingEngineHost.LoadIncludeText(string requestFileName, out string content, out string location)
        {
            content = System.String.Empty;
            location = System.String.Empty;

            //If the argument is the fully qualified path of an existing file,
            //then we are done.
            //----------------------------------------------------------------
            if (File.Exists(requestFileName))
            {
                content = File.ReadAllText(requestFileName);
                return true;
            }
            //This can be customized to search specific paths for the file.
            //This can be customized to accept paths to search as command line
            //arguments.
            //----------------------------------------------------------------
            else
            {
                return false;
            }
        }

        void ITextTemplatingEngineHost.LogErrors(CompilerErrorCollection errors)
        {
            _errors = errors;
            foreach (var e in errors)
            {
                Console.WriteLine(e);
            }
        }

        AppDomain ITextTemplatingEngineHost.ProvideTemplatingAppDomain(string content)
        {
            return AppDomain.CurrentDomain;
        }

        public virtual string ResolveAssemblyReference(string assemblyReference)
        {
            if (File.Exists(assemblyReference))
            {
                return assemblyReference;
            }

            try
            {
                // TODO: This is failing to resolve partial assembly names (e.g. "System.Xml")
                var assembly = Assembly.Load(assemblyReference);

                if (assembly != null)
                {
                    return assembly.Location;
                }
            }
            catch (FileNotFoundException)
            {
            }
            catch (FileLoadException)
            {
            }
            catch (BadImageFormatException)
            {
            }

            return string.Empty;
        }

        Type ITextTemplatingEngineHost.ResolveDirectiveProcessor(string processorName)
        {
            //This host will not resolve any specific processors.
            //Check the processor name, and if it is the name of a processor the 
            //host wants to support, return the type of the processor.
            //---------------------------------------------------------------------
            if (string.Compare(processorName, "T4VSHost", StringComparison.OrdinalIgnoreCase) == 0)
            {
                return typeof(FallbackT4VSHostProcessor);
            }
            //This can be customized to search specific paths for the file
            //or to search the GAC
            //If the directive processor cannot be found, throw an error.
            throw new Exception("Directive Processor not found");
        }

        string ITextTemplatingEngineHost.ResolveParameterValue(string directiveId, string processorName, string parameterName)
        {
            return string.Empty;
        }

        string ITextTemplatingEngineHost.ResolvePath(string path)
        {
            if (!Path.IsPathRooted(path) && Path.IsPathRooted(TemplateFile))
            {
                return Path.Combine(Path.GetDirectoryName(TemplateFile), path);
            }

            return path;
        }

        void ITextTemplatingEngineHost.SetFileExtension(string extension)
        {
            FileExtension = extension;
        }

        void ITextTemplatingEngineHost.SetOutputEncoding(Encoding encoding, bool fromOutputDirective)
        {
            OutputEncoding = encoding;
        }
    }
}
