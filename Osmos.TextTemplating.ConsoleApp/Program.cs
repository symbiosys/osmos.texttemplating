﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Osmos.TextTemplating.ConsoleApp
{
    public class TestClass {
        public string Name { get; set; }
        public string SomeString() {
            return "Some string";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var options = new TextTempateHostOptions
            {
                StandardAssemblyReferences = new List<string>{
                    typeof(TextTempateHost).Assembly.Location,
                    Assembly.GetExecutingAssembly().Location
                },
                StandardImports = new List<string>{
                    "Osmos.TextTemplating",
                    "Osmos.TextTemplating.ConsoleApp"
                },
                Object = new TestClass
                            { 
                                Name = "Object name"
                            }
            };

            var host = new TextTempateHost(options);

            string rootPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName).FullName;
            string templateFilePath = Path.Combine(rootPath, @"Osmos.TextTemplating.ConsoleApp\Templates\TextTemplate2.tt");

            var task1 = Task.Run(async () => await TextTemplateEngine.GetTextAsync(templateFilePath, host));
            task1.Wait();

            Console.WriteLine(task1.Result);

            var task2 = Task.Run(async () => await TextTemplateEngine.GetFileAsync(
                                                    templateFilePath,
                                                    @"C:\Users\DaFrisky\Desktop", 
                                                    "TestTemplateResult",
                                                    host));
            task2.Wait();
        }
    }
}
